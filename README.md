General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Sublime or any other text editor.
3. Have the input file in the required format.
4. Run the script using the following commands 
    * `python nblearn.py /path/to/input`
    * `python nbclassify.py /path/to/input`

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Naive Bayes Classifier](https://en.wikipedia.org/wiki/Naive_Bayes_classifier)
* [Porter's Stemmer](http://tartarus.org/martin/PorterStemmer/def.txt)
* [Text Classification and Naive Bayes](http://nlp.stanford.edu/IR-book/pdf/13bayes.pdf)

Table of Contents
====================
----------------------------------
[TOC]

Hotel Review Classification - Naive Bayes Classifier
=======================================================
--------------------------------------------------------------

## Problem Statement

> This is a Naive Bayes classifier to identify hotel reviews as either truthful or deceptive, and either positive or negative. We have used the word tokens as features for classification.

> **There are two programs:** 

* nblearn.py which will learn a naive Bayes model from the training data 

* nbclassify.py will use the model to classify new data.

> **The learning program is  invoked in the following way:**

> The argument is the directory of the training data; the program will learn a naive Bayes model, and write the model parameters to a file called nbmodel.txt. 

> **The classification program will be invoked in the following way:**

> The argument is the directory of the test data; the program will read the parameters of a naive Bayes model from the file nbmodel.txt, classify each file in the test data, and write the results to a text file called nboutput.txt in the following format:

> label_a label_b path1

> label_a label_b path2 

> ⋮

> In the above format, label_a is either “truthful” or “deceptive”, label_b is either “positive” or “negative”, and pathn is the path of the text file being classified.

## Data

> **The data folders are of the following format:**

* A top-level directory with two sub-directories, one for positive reviews and another for negative reviews (plus license and readme files which is not needed).

* Each of the subdirectories contains two sub-directories, one with truthful reviews and one with deceptive reviews.

* Each of these subdirectories contains four subdirectories, called “folds”.

* Each of the folds contains 80 text files with English text (one review per file).

* Development data. 
> While developing the programs, we reserve some of the data as development data in order to test the performance of our programs. We used folds 2, 3, and 4 as training data, and fold 1 as development data: that is, it will run nblearn.py on a directory containing only folds 2, 3, and 4, and it will run nbclassify.py on a directory containing only fold 1.

* For stemming the tokens we have implemented the Porter's Stemmer

Edit Log
=======
-----------------
## Complete Creation
> Date: 09/02/16
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
> * Functions 
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 09/02/16