import sys
import glob
import os
import string
import math
from fractions import Fraction
from decimal import Decimal, getcontext
getcontext().prec = 400

model = {}
postruth = {}
posdeceit = {}
negtruth = {}
negdeceit = {}

wordcount_postruth = 0
wordcount_posdeceit = 0
wordcount_negtruth = 0
wordcount_negdeceit = 0

key = "postruth"
value = postruth
model[key] = value

key = "posdeceit"
value = posdeceit
model[key] = value

key = "negtruth"
value = negtruth
model[key] = value

key = "negdeceit"
value = negdeceit
model[key] = value

print model
#path = "/Users/siddhigupta/Documents/StudyMaterial/2nd Semester/op_spam_train/"
path  = sys.argv[1]
for file in glob.glob(path + "*"):
	if os.path.isdir(file):
		print os.getcwd
		print file
		for subfile in glob.glob(file + "/*"):
			if os.path.isdir(subfile):
				print os.getcwd
				print subfile
				for subsubfile in glob.glob(subfile + "/*"):
					if os.path.isdir(subsubfile):
						print os.getcwd
						print subsubfile
						dictname = ""
						if subsubfile.find("positive") != -1 and subsubfile.find("deceptive") != -1:
							print "I came here"
							dictname = "posdeceit"
						elif subsubfile.find("positive") != -1 and subsubfile.find("truthful") != -1:
							dictname = "postruth"
						elif subsubfile.find("negative") != -1 and subsubfile.find("truthful") != -1:
							dictname = "negtruth"	
						else:
							dictname = "negdeceit"	
							
						print dictname	
						
						for subsubsubfile in glob.glob(subsubfile + "/*"):
							print subsubsubfile
							f = open(subsubsubfile, 'r')
							review = f.readline()
							for p in string.punctuation:
								review = review.replace(p, " ")
							review = ''.join([d for d in review if not d.isdigit()])
							print review
							review = review.rstrip('\r\n').lstrip('\r\n').split(" ")
							for word in review:
								#if word == 'the' or word == 'rd' or word == 'rm' or word == 're' or word == '' or word == ' ' or word == 'nd' or word == 'st' or word == 'se' or word == 'sf' or word == 'san':
								#	continue			
								key = word.lower()
								if key == "" or key =="\n":
									continue
								value = 1
								if dictname == "posdeceit":
									wordcount_posdeceit += 1
								elif dictname == "postruth":
									wordcount_postruth += 1
								elif dictname == "negtruth":
									wordcount_negtruth += 1
								else:
									wordcount_negdeceit += 1
								if key in eval(dictname):
									c = eval(dictname)[key]
									c += 1
									eval(dictname)[key] = c
								else:
								
									eval(dictname)[key] = value
print model
print "difference0"
a = set(postruth.keys())
b = set(posdeceit.keys()) 
c = set(negdeceit.keys()) 
d = set(negtruth.keys()) 
a = (((b - a)).union((c - a))).union((d - a))
b = (((a - b)).union((c - b))).union((d - b))
c = (((a - c)).union((b - c))).union((d - c))
d = (((a - d)).union((b - d))).union((c - d))
print a
print len(a)
for key in postruth:
	postruth[key] = postruth[key] + 1
for ele in a:
	key = ele
	value = 1
	postruth[key] = value
print b
print len(b)
for key in posdeceit:
	posdeceit[key] = posdeceit[key] + 1
for ele in b:
	key = ele
	value = 1
	posdeceit[key] = value
print c
print len(c)
for key in negdeceit:
	negdeceit[key] = negdeceit[key] + 1
for ele in c:
	key = ele
	value = 1
	negdeceit[key] = value
print d
print len(d)
for key in negtruth:
	negtruth[key] = negtruth[key] + 1
for ele in d:
	key = ele
	value = 1
	negtruth[key] = value
print "model2"
print model
print "wordcount"	
print wordcount_posdeceit
print wordcount_postruth
print wordcount_negtruth
print wordcount_negdeceit

wordcount_posdeceit += len(b)
wordcount_postruth += len(a)
wordcount_negtruth += len(d)
wordcount_negdeceit	+= len(c)				
print "wordcount"	
print wordcount_posdeceit
print wordcount_postruth
print wordcount_negtruth
print wordcount_negdeceit
								
prior_postruth = {}
prior_posdeceit = {}
prior_negtruth = {}
prior_negdeceit = {}	

for key in postruth:
	print postruth[key]
	print wordcount_postruth
	value = math.log(Decimal(postruth[key]) / Decimal(wordcount_postruth))
	print value
	prior_postruth[key] = value
print prior_postruth

for key in posdeceit:
	print posdeceit[key]
	print wordcount_posdeceit
	value = (math.log(Decimal(posdeceit[key]) / Decimal(wordcount_posdeceit)))
	prior_posdeceit[key] = value
print prior_posdeceit	

for key in negtruth:
	print negtruth[key]
	print wordcount_negtruth
	value = (math.log(Decimal(negtruth[key]) / Decimal(wordcount_negtruth)))
	prior_negtruth[key] = value
print prior_negtruth

for key in negdeceit:
	print negdeceit[key]
	print wordcount_negdeceit
	value = (math.log(Decimal(negdeceit[key]) / Decimal(wordcount_negdeceit)))
	prior_negdeceit[key] = value
print prior_negdeceit

f1 = open('model.txt', 'w')
f1.write("prior_postruth\n")
f1.write(str(prior_postruth))
f1.write("\n")
f1.write("prior_posdeceit\n")
f1.write(str(prior_posdeceit))
f1.write("\n")
f1.write("prior_negtruth\n")
f1.write(str(prior_negtruth))
f1.write("\n")
f1.write("prior_negdeceit\n")
f1.write(str(prior_negdeceit))
f1.close()
