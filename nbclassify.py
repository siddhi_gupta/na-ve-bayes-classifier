import sys
import glob
import os
import string
import math
from fractions import Fraction
from decimal import Decimal, getcontext
getcontext().prec = 400

f = open('model.txt', 'r')
name1 = f.readline()
print name1
s = f.readline()
prior_postruth = eval(s)
print prior_postruth

#f1 =open('accuracy.txt' , 'w')
f1 =open('nboutput.txt' , 'w')
wordcount_postruth = 0
wordcount_posdeceit = 0
wordcount_negtruth = 0
wordcount_negdeceit = 0

name1 = f.readline()
print name1
s = f.readline()
prior_posdeceit = eval(s)
print prior_posdeceit

name1 = f.readline()
print name1
s = f.readline()
prior_negtruth = eval(s)
print prior_negtruth

name1 = f.readline()
print name1
s = f.readline()
prior_negdeceit = eval(s)
print prior_negdeceit

class_probability = math.log(0.25)
print class_probability
#path = "/Users/siddhigupta/Documents/StudyMaterial/2nd Semester/op_spam_test/"
path  = sys.argv[1]
for file in glob.glob(path + "*"):
	if os.path.isdir(file):
		print os.getcwd
		print file
		for subfile in glob.glob(file + "/*"):
			if os.path.isdir(subfile):
				print os.getcwd
				print subfile
				for subsubfile in glob.glob(subfile + "/*"):
					if os.path.isdir(subsubfile):
						print subsubfile
						
						for subsubsubfile in glob.glob(subsubfile + "/*"):
							print subsubsubfile
							f = open(subsubsubfile, 'r')
							review = f.readline()
							for p in string.punctuation:
								review = review.replace(p, " ")
							review = ''.join([d for d in review if not d.isdigit()])
							print review
							review = review.rstrip('\n').split(" ")
							print review
							postruth_probab = class_probability
							for word in review:
								#if word == 'the' or word == 'rd' or word == 'rm' or word == 're' or word == '' or word == ' ' or word == 'nd' or word == 'st' or word == 'se' or word == 'sf' or word == 'san':
									#continue	
								if word == "" or word =="\n":
									continue	
								if word not in prior_postruth:
									continue
								postruth_probab += prior_postruth[word]
							print postruth_probab
								
							posdeceit_probab = class_probability
							for word in review:
								if word not in prior_posdeceit:
									continue
								posdeceit_probab += prior_posdeceit[word]
							print posdeceit_probab	
								
							negtruth_probab = class_probability
							for word in review:
								if word not in prior_negtruth:
									continue
								negtruth_probab += prior_negtruth[word]
							print negtruth_probab
								
							negdeceit_probab = class_probability
							for word in review:
								if word not in prior_negdeceit:
									continue
								negdeceit_probab += prior_negdeceit[word]
							print negdeceit_probab	
							
							list = [postruth_probab, posdeceit_probab, negtruth_probab, negdeceit_probab]
							c = max(list)
							print c
							if c == postruth_probab:
								#f1.write("postruth\n")
								f1.write("truthful positive " + subsubsubfile + "\n")
								wordcount_postruth = wordcount_postruth + 1
							elif c == posdeceit_probab:
								#f1.write("posdeceit\n")
								f1.write("deceptive positive " + subsubsubfile + "\n")
								wordcount_posdeceit = wordcount_posdeceit + 1
							elif c == negtruth_probab:
								#f1.write("negtruth\n")
								f1.write("truthful negative " + subsubsubfile + "\n")
								wordcount_negtruth = wordcount_negtruth + 1
							else:
								#f1.write("negdeceit\n")
								f1.write("deceptive negative " + subsubsubfile + "\n")
								wordcount_negdeceit = wordcount_negdeceit + 1
								
'''print "Words Classified as"								
print "Posdeceit"
print wordcount_posdeceit
print "Postruth"
print wordcount_postruth
print "Negtruth"
print wordcount_negtruth
print "Negdeceit"
print wordcount_negdeceit								
'''							
f1.close()

'''
count = 0
f1 = open('accuracy.txt', 'r')
f2 = open('finalresult.txt', 'r')
i = 0
while i != 320:
	s1 = f1.readline()
	s2 = f2.readline()
	i = i + 1
	if s1 == s2:
		count = count + 1
print count		
print "accuracy"
ac = count/320.0
print ac	
'''